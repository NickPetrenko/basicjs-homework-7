//1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, 
//яка приймає рядок str і повертає true, якщо рядок є паліндромом.

function isPalindrome(input) {
    var str = String(input);
    var cleanedStr = str.replace(/[^\w]/g, '').toLowerCase();
    
    var reversedStr = cleanedStr.split('').reverse().join('');
  
    return cleanedStr === reversedStr;
}

var userInput = prompt("Введіть рядок для перевірки:");
  
console.log(isPalindrome(userInput));



//2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, 
//який потрібно перевірити, максимальну довжину і повертає true, якщо 
//рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. 
//Ця функція стане в нагоді для валідації форми.

function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

var userInput = prompt("Введіть рядок для перевірки:");

var maxLengthInput = parseInt(prompt("Введіть максимальну довжину рядка:"));

console.log(checkStringLength(userInput, maxLengthInput));



//3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте 
//дату народження користувача через prompt. Функція повина повертати значення 
//повних років на дату виклику функцію.

function calculateAge(dateOfBirth) {
    var currentDate = new Date(); // Поточна дата
    var birthDate = new Date(dateOfBirth); // Дата народження
  
    var age = currentDate.getFullYear() - birthDate.getFullYear();
    var monthDiff = currentDate.getMonth() - birthDate.getMonth();
    var dayDiff = currentDate.getDate() - birthDate.getDate();
  
    // Перевіряємо, чи користувач вже відсвяткував день народження в поточному році
    if (monthDiff < 0 || (monthDiff === 0 && dayDiff < 0)) {
      age--;
    }
    return age;
}
  
// Отримуємо дату народження користувача через prompt
var userInput = prompt("Введіть дату народження у форматі 'MM/DD/YYYY':");
  
// Викликаємо функцію calculateAge з введеною датою і виводимо результат
console.log(calculateAge(userInput));